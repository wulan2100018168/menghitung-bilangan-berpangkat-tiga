;Program ini menghitung bilangan berpangkat tiga
;dengan satu buah input variable bebas yang dapat bernilai positifmaupun negatif
;output luas sebagai variable terikat

INCLUDE 'emu8086.inc'
#MAKE COM#
ORG 100H
JMP MULAI
PSN1   DB "MENGHITUNG BILANGAN BERPANGKAT TIGA $"
PSN2   DB "MASUKKAN NILAI : $"
PSN3   DB " "
PSN4   DB "HASIL : $"
PSN5   DB "TEKAN SEMBARANG UNTUK KELUAR $"

ANGKA  DW ?
ARE    DW ?

MULAI:
LEA DX, PSN1
MOV AH, 9
INT 21H

LEA DX,PSN2
MOV AH, 9
INT 21H

CALL SCAN_NUM

MOV ANGKA,CX

PUTC 13
PUTC 10


MOV AX,ANGKA
IMUL AX
IMUL AX
DIV ANGKA
PUSH AX

PUTC 13
PUTC 10 

LEA DX, PSN4
MOV AH, 9
INT 21H

POP AX
MOV ARE, AX

CALL PRINT_NUM

PUTC 13
PUTC 10
LEA DX, PSN5
MOV AH, 9 
INT 21H

MOV AH,0
INT 16H

RET

DEFINE_SCAN_NUM
DEFINE_PRINT_NUM
DEFINE_PRINT_NUM_UNS
END

